from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)

    return {"picture_url": content["photos"][0]["src"]["original"]}


def get_weather_data(city, state):
    l_url = "http://api.openweathermap.org/geo/1.0/direct?"
    l_params = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(l_url, params=l_params)
    location = json.loads(response.content)
    lat = location[0]["lat"]
    lon = location[0]["lon"]

    w_url = "https://api.openweathermap.org/data/2.5/weather?"
    w_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    w_response = requests.get(w_url, params=w_params)
    weather = json.loads(w_response.content)
    return {
        "temperature": weather["main"]["temp"],
        "description": weather["weather"][0]["description"],
    }

    # url = "https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}"
    # headers = {"Authorization": OPEN_WEATHER_API_KEY}
    # response = requests.get(url, headers=headers)
    # params =
    # weather = json.loads(response.content)
